CC = g++
CCOPT = -O
GSLLIB = -static -L/usr/lib -lgsl -lgslcblas -lm
HEADERS = emf.h
OBJECTS = func.o chisq.o aniso.o grid.o mle.o \
	gmin.o brent.o conjgr.o simplex.o powell.o \
	levmar.o anneal.o fitmodel.o rjmap.o  \
	file.o data.o print.o main.o \

eMF : $(OBJECTS)
	$(CC) $(OBJECTS) $(GSLLIB) -o $@

%.o : %.c $(HEADERS)
	$(CC) $(CCOPT) -c $<
install :
	cp eMF ~/bin/eMFv1
clean :
	rm -f *.o eMF
