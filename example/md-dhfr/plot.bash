#!/bin/bash
out="eMF-legacy.out"
eMFget.pl $out S2    > ${out}.S2
eMFget.pl $out Rex   > ${out}.Rex
eMFget.pl $out model > ${out}.model
# For comparison with MD (Jianhan et al.)
# effective te = (S2f-S2)/(1.0-S2) * te
# effective te(err) = (S2f-S2)/(1.0-S2) * te(err)
eMFget.pl $out \
| awk '/^[^#]/{S2=$2;S2f=$6;fac=(S2f-S2)/(1.0-S2); print $1,1000*fac*$4,1000*fac*$5}' > ${out}.te
