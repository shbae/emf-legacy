#!/bin/bash
cat << END_OF_HEADER
# eMF version 2.0 input data

# syntax: CONST <constant title> Value
# user accessible constants
#  gamma_h, gamma_x : gyromagnetic ratios for H and X
#  r_xh             : bond length betwen H and X
#  csa_x            : chemical shift anisotropy of X
#  beta             : angle between bond H-X and principal axis of CSA tensor

CONST gamma_h	  26.7519  # (10^7 rad s^-1 T^-1)
CONST gamma_x	  -2.71    # (10^7 rad s^-1 T^-1)
CONST r_xh         0.997   # (A)
CONST csa_x	-170       # (ppm)
CONST beta 	  18       # (deg)

# syntax: METHOD <minimizer name> [MC|trials]
# availabel methods:
#
#   GRID            : grid search
#   QRAND [#]       : quasi-random search (GNU Scientific Library)
#   RAND [#]        : random search (GNU Scientific Library)
#   RANDSIMPLEX [#] : random simplex search (GNU Scientific Library)
#   #= number of trials (optional)
#
#   SIMPLEX [#]     : simplex (GNU Scientific Library)
#   LEVMAR  [#]     : Levenberg-Marquardt (GNU Scientific Library)
#   LEVMAR-ML [#]   : Levenberg-Marquardt (Manolis Lourakis)
#   LEVMAR-ML-BC [#]: Levenberg-Marquardt box constrained (Manolis Lourakis)
#   BCOOL [#]       : box constrained (Open Optimization Library)
#   CONJGR [#]      : conjugated gradient (GNU Scientific Library)
#   SA [#]          : simulated annealing (GNU Scientific Library)
#   #= number of Monte Carlo simulations (optional)
 
# recommended : GRID -> SIMPLEX -> LEVMAR
#   parameters are searched using grid and simplex (non-gradient method) 
#   then minimized using gradient-based algorithms such as LEVMAR

METHOD GRID
METHOD SIMPLEX
METHOD LEVMAR
#METHOD LEVMAR-ML
#METHOD LEVMAR-ML-BC
#METHOD BCOOL
#METHOD CONJGR

# syntax: VECTOR <atom name 1> <atom name 2> <pdb filename>
#   bond vector information for axially symmetric diffusion tensor 
VECTOR N H 1rx7.rot.pdb

# syntax: FUNCTION <function name> <active set flag> [<active set flag> ... ]
#   available functions
#   LSI   : extended Lipari-Szabo Isotropic diffusion tensor
#           [S2s, te, Rex, S2f, tc]
#   LSX   : extended Lipari-Szabo aXially symmetric diffusion tensor 
#           [S2s, te, Rex, S2f, tc, Dr, phi, theta]
#   LSICC : extended Lipari-Szabo Isotropic diffusion /w Cross Correlation (CC)
#           [S2s, te, Rex, S2f, S2cd, beta, tc]
#   LSXCC : extended Lipari-Szabo aXially symmetric diffusion /w CC
#           [S2s, te, Rex, S2f, S2cd, beta, tc, Dr, phi, theta]
#   CC    : use Cole-Cole spectral density function
#           [tc, e, S2, te, Rex]
#   Loc2  : Local diffusion tensor with two correlation times
#           [tc1, tc2, c, S2, te, Rex]
#   LocX1 : Local aXially symmetric type 1
#           [Dpar, Dper, cos2, S2, te, Rex]
#   LocX2 : Local aXially symmetric type 1
#           [tiso, Dr, cos2, S2, te, Rex]
#   Exp   : exponential decay
#           [R, a, b]
#   Tanh  : hyperbolic tangent
#           [eta_xy]
#         
#   function name is followed by bitwise flag for each parameter
#       1 : to be fitted
#       0 : fixed

FUNCTION LSX 10000000 11000000 10100000 11100000 11010000 

# i.e. for extended Lipari-Szabo models /w axially symmetric tensor (LSX)
# model 1 : [S2s,  -,   -,   -, -, -, -, -] = 10000000
# model 2 : [S2s, te,   -,   -, -, -, -, -] = 11000000
# model 3 : [S2s,  -, Rex,   -, -, -, -, -] = 10100000
# model 4 : [S2s, te, Rex,   -, -, -, -, -] = 11100000
# model 5 : [S2s, te,   -, S2f, -, -, -, -] = 11010000

# syntax: SET <function title> <parameter title> Value
# syntax: SET <function title> <parameter title> Lower Upper
# syntax: SET <function title> <parameter title> Lower Upper Grids
#   parameters are fitted within lower boundary, upper boundary,
#   and number of grids (optional)
SET LSX S2s	0  1 20
SET LSX S2f	0  1 20
SET LSX te	0  3 60
SET LSX Rex	0 10 10 
#   parameters are fixed by giving a single value
SET LSX tc      8.63
SET LSX	Dr      1.195
SET LSX	phi    -2.385
SET LSX	theta   0

# syntax: RESID <residue number> [flag]
#   flag 1 : used for DIFFUSION
#        0 : not used for DIFFUSION

# syntax: DATA  <data type> <x> <y> <dy(error)>
#   data type is handled within each function and can be any integer
#   currently,
#   type 0 : R1 (1/s)
#        1 : R2 (1/s)
#        2 : heteronuclear NOE
#        3 : transverse DD-CSA cross correlation rate (eta_xy) (1/s)
#        4 : longitudinal DD-CSA cross correlation rate (eta_z) (1/s)

END_OF_HEADER

eMFprep.pl \
	-d 0 500.38 wt_R1_500MHz.dat \
	-d 0 600.13 wt_R1_600MHz.dat \
	-d 1 500.38 wt_R2_500MHz.dat \
	-d 1 600.13 wt_R2_600MHz.dat \
	-d 1 750.13 wt_R2_750MHz.dat \
	-d 2 500.38 wt_NOE_500MHz.dat \
	-d 2 600.13 wt_NOE_600MHz.dat
